# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login as base_login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import redirect, render
from models import *


def login(request):
    if request.method == 'GET':
        return render(request, 'login.html')
    elif request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            base_login(request, user)
            return redirect('/blog/')
        else:
            return HttpResponse('Ошибка авторизации')


@login_required
def index(request):
    if request.method == 'GET':
        return render(request, 'index.html', {})
    elif request.method == 'POST':
        username = request.POST.get('username')
        user_subscribe = User.objects.get(username=username)
        try:
            blog_subscribe = Blog.objects.get(author=user_subscribe)
        except Blog.DoesNotExist:
            return HttpResponse('Такого юзера еще не придумали')
        user = request.user
        try:
            blog = Blog.objects.get(author=user)
        except Blog.DoesNotExist:
            blog = Blog.objects.create(author=user)
        blog.subscribe.add(blog_subscribe)
        return redirect('/')


@login_required
def blog(request):
    user = request.user
    if request.method == 'GET':
        try:
            blog = Blog.objects.get(author=user)
        except Blog.DoesNotExist:
            blog = Blog.objects.create(author=user)

        posts = Post.objects.filter(blog=blog).order_by('-created_at')
        return render(request, 'blog.html', {'user': user, 'posts': posts})
    elif request.method == 'POST':
        title = request.POST.get('title')
        text = request.POST.get('text')
        try:
            blog = Blog.objects.get(author=user)
        except Blog.DoesNotExist:
            return HttpResponse('error')

        Post.objects.create(title=title, text=text, blog=blog)
        return redirect('/blog/')


@login_required
def blogs(request):
    user = request.user
    try:
        blog = Blog.objects.get(author=user)
    except Blog.DoesNotExist:
        blog = Blog.objects.create(author=user)

    if request.method == 'GET':
        posts = Post.objects.filter(blog__in=blog.subscribe.all())
        return render(request, 'blogs.html', {'posts': posts, 'user': user})
    elif request.method == 'POST':
        post_id = request.POST.get('post_id')
        post = Post.objects.get(id=post_id)
        post.read.add(user)
        return redirect('/blogs/')
