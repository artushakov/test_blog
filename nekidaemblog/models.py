# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import User
from django.core.mail import send_mail


@python_2_unicode_compatible
class Blog(models.Model):
    author = models.OneToOneField(User, verbose_name=u'Автор')
    subscribe = models.ForeignKey('self', blank=True)

    def __str__(self):
        return u'Блог пользователя {}'.format(self.author.username)
        
        
@python_2_unicode_compatible
class Post(models.Model):
    blog = models.ForeignKey(Blog, null=True)
    title = models.CharField(max_length=100, verbose_name=u'Заголовок')
    content = models.TextField(verbose_name=u'Текст')
    read = models.ManyToManyField(User, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name=u'Создан')

    def __str__(self):
        return self.title

    def save(self):
        mails = []
        for blog in Blog.objects.filter(author=user):
            mails.append(blog.author.email)
        send_mail('Добалена запись в блог', 'Добавлена запись в блог', 'from@example.com', mails,
                      fail_silently=False)
        super(Post, self).save(*args, **kwargs)
